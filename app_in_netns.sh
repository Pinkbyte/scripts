#!/bin/bash
[[ -z $1 ]] && echo >&2 "Error: not enough arguments, application to run is not defined" && exit 1
./netns_shell.sh su -c \'${1}\' -l pinkbyte
