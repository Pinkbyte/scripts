#!/bin/sh
# Script generates 'stage4' - compressed tarball of system files
# Copyright: Sergey Popov <admin@pinkbyte.ru>
# Distributed under GNU GPL
tar cjpf /mnt/files/incoming/pinkbyte-desktop-stage-i7.tar.bz2 / -X backup_script.exclude
