#!/bin/sh
WORK_CONFIG=`cd /etc/portage;find ./ -type f`
HOME_CONFIG=`cd ~/dev/portage-home;find ./ -type f`

HOME_PATH="/home/pinkbyte/dev/portage-home"
WORK_PATH="/etc/portage"
COMMON_PATH="/home/pinkbyte/dev/portage-common"

rm -f ${COMMON_PATH}/same_name_different_content.txt &>/dev/null

for i in $WORK_CONFIG; do
	if [ -e ${HOME_PATH}/${i} ]; then
		HOME_CSUM=`md5sum ${HOME_PATH}/${i} | awk '{ print $1; }'`
		WORK_CSUM=`md5sum ${WORK_PATH}/${i} | awk '{ print $1; }'`
		if [ "$HOME_CSUM" = "$WORK_CSUM" ]; then
			mkdir ${COMMON_PATH}/`dirname ${i}` -p
			cp ${HOME_PATH}/${i} ${COMMON_PATH}/`dirname ${i}`
		else
			echo ${i} >> ${COMMON_PATH}/same_name_different_content.txt
		fi
	fi
done
