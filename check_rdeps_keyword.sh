#!/bin/bash
# Script for checking keyword level on reverse dependencies of specific package
# Copyright: Sergey Popov <admin@pinkbyte.ru>
# Distributed under GNU GPL v2 or higher

if [[ -z "$1" ]]; then
	echo "Error: you need to supply package in form category/package as a first argument for this script" 1>&2
	exit 1
fi
if [[ -z "$2" ]]; then
	echo "Error: you need to supply keyword to check for 'arch' for stable or '~arch' for unstable(without quotes) as a second argument for this script" 1>&2
	exit 1
fi
PKG=$1
KV=$2

for STRING in $(wget http://qa-reports.gentoo.org/output/genrdeps/rindex/${PKG} -q -O -); do
	# Skip blocked atoms
	[[ "${STRING/[B]}" = "${STRING}" ]] || continue
	# Drop all things except category and package name
	while read CATEGORY PACKAGE VERSION OTHER; do
		echo $CATEGORY/$PACKAGE
	done <<< $(qatom $STRING)
done | sort | uniq | while read PN; do
# For proper grepping of stable keywords
	if [[ "$KV" = ~* ]]; then
		grep --include=*.ebuild -lr ${KV} /usr/portage/${PN}
	else
		grep --include=*.ebuild -lr " ${KV}" /usr/portage/${PN}
	fi
done
