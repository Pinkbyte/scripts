#!/bin/bash

# Read from standard input, leave only russian characters and hyphen
read_input() {
	local IFS='" ;'
	while read LINE; do
		for WORD in ${LINE}; do
			WORDS+=($(echo ${WORD} | sed -e 's/[^а-яА-Я-]//g'))
		done
	done
}

# Check for element in array
contains() {
	local e match="$1"
	shift
	for e; do [[ "$e" == "$match" ]] && return 0; done
	return 1
}

pick_unique_random() {
	local maxtries=50
	local tries=${maxtries}
	while [[ ${tries} -ne 0 ]]; do
		let tries=tries-1
		RINDEX=$((RANDOM % ${#WORDS[@]}))
		contains "${RINDEX}" "${PICKED[@]}"
		if [[ $? -ne 0 ]]; then
			echo -n $RINDEX
			return 0
		fi
	done
	return 1
}

WORDS=()
PICKED=()
read_input

# Output 5 strings
for i in $(seq 1 5); do
	# Output 5 words in string
	for j in $(seq 1 5); do
		# First - try to pick unique random index in WORDS array
		RINDEX=$(pick_unique_random)
		# If it fails (for example - input is too small - pick non-unique random)
		if [[ $? -ne 0 ]]; then
			RINDEX=$((RANDOM % ${#WORDS[@]}))
		fi
		PICKED+=(${RINDEX})
		echo -n "${WORDS[${RINDEX}]} "
	done
	echo
done
