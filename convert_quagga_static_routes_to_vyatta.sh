#!/bin/bash
# Pipeline scripts to convert Quagga static routes statements from zebra.conf
# to Vyatta config script
# Copyright: Sergey Popov aka Pinkbyte <admin@pinkbyte.ru>
# Distributed under GNU GPLv2

# Warning: script does not validate IP address parameters, it presumes that input is copied
# from valid Quagga config file
# Warning 2: interface routes will NOT be converted

echo '#!/bin/vbash'
echo 'source /opt/vyatta/etc/functions/script-template'
echo 'configure'
sed -e 's:ip route \([0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}/[0-9]\{1,2\}\) \([0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\):set protocols static route \1 next-hop \2:'
echo 'commit'
