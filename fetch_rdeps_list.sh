#!/bin/bash
if [[ -z "$1" ]]; then
	echo "Error: you need to supply package in form category/package for this script" 1>&2
	exit 1
fi
PKG=$1
for STRING in $(wget http://qa-reports.gentoo.org/output/genrdeps/rindex/${PKG} -q -O -); do
	# Skip blocked atoms
	[[ "${STRING/[B]}" = "${STRING}" ]] || continue
	# Drop all things except category and package name
	while read CATEGORY PACKAGE VERSION OTHER; do
		echo $CATEGORY/$PACKAGE
	done <<< $(qatom $STRING)
done | sort | uniq
