#!/bin/sh
# Script generates psk.txt file in current directory for /24 subnet with one key for each host
# Copyright: Sergey Popov <admin@pinkbyte.ru>
# Distributed under GNU GPL

key="111";
mynet="10.35.100.";
file="psk.txt";

n=1;

while ( [ $n -lt 255 ]; ); do
	if [ $n = 1 ];  then
		echo -n ""
	else
	if [ $n = 2 ]; then
		echo -n ""
	else
        if [ $n = 3 ]; then
                echo -n ""
        else
        if [ $n = 4 ]; then
                echo -n ""
        else
        if [ $n = 7 ]; then
                echo -n ""
        else
        if [ $n = 250 ]; then
                echo -n ""
        else
		echo "$mynet$n	$key" >> $file
	fi
	fi
	fi
	fi
	fi
	fi
	n=`expr $n + 1`
done
rm psk.txt
