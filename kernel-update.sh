#!/bin/bash

IS_KERNEL_DIR="$(pwd | grep /usr/src/linux-)"
if [ -z "$IS_KERNEL_DIR" ]; then
	echo "Это не директория с исходниками ядра, сборка невозможна!"
	exit 1
fi

KERNEL_VER="$(pwd | sed -e 's:/usr/src/linux-\(.*\):\1:')"

echo "Текущая Версия ядра - `uname -r`"
echo "Версия собираемого ядра - ${KERNEL_VER}"
echo -n "Продолжить сборку(y/N): "
read ANSWER

if [ "$ANSWER" != "y" ]; then
	echo "Пока-пока..."
	exit 2
else
	PREVIOUS_KERNEL_DIR="$(eselect --brief --color=n kernel show | tr -d ' ')"
	eselect kernel set linux-${KERNEL_VER}
	if [[ $? -ne 0 ]]; then
		echo "Не удалось установить символьную ссылку на новую версию исходников ядра, плак-плак..."
		eselect kernel set ${PREVIOUS_KERNEL_DIR}
		exit 3
	fi
	zcat /proc/config.gz > .config
	make -j8
	mount -o remount,rw /boot
	make modules_install install
	genkernel initramfs
	emerge @module-rebuild
fi
