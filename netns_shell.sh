#!/bin/bash

if [[ -z "$1" ]]; then
	PROG=bash
else
	PROG=$*
fi

DEFAULT_NS="TEST1"

choose_namespace() {
	local CHOICE
	echo 'Choose network namespace from (default one is "'${DEFAULT_NS}'"):' >&2
	let a=0
	while read LINE;
	do
		let a=a+1
		NAMESPACES+=( "${LINE}" )
		echo "${LINE}" >&2
	done < <(ip netns show | awk '{ print $1; }' )
	echo -n "Your choice: " >&2
	read CHOICE
	echo $CHOICE
}

CHOICE=$(choose_namespace)
if [ -z "${CHOICE}" ]; then
	CHOICE="${DEFAULT_NS}"
fi
export PROMPT_COMMAND="PS1=\"(netns ${CHOICE}) \${PS1}\";unset PROMPT_COMMAND"
echo ip netns exec ${CHOICE} ${PROG}
eval "ip netns exec ${CHOICE} ${PROG}"
exit $?
