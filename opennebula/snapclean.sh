#!/bin/bash
# Check if requested parameters supplied
[ $# -lt 1 ] && exit 1
[ $# -gt 2 ] && exit 1
# Check if script is executed from right username
[ "${USER}" = oneadmin ] || exit 2

if [ $# -ne 2 ]; then
	echo >&2 "Error: you must define how many snapshots to keep"
	exit 3
fi

if ! [[ $2 =~ ^[0-9]+$ ]]; then
	echo >&2 "Error: second parameter(number of snapshots to keep) should be positive numeric value"
	exit 4
fi

declare -i NUM=$2
if [ ${NUM} -gt 10 ]; then
	echo >&2 "Error: number of snapshots to keep is incorrect - choose value from 0 to 10"
	exit 5
fi

# Snapshot will be done only for first attached hard drive
VM_NAME="$1"
IMG_NAME="$(onevm show ${VM_NAME} | awk '/sda/{ print $4; }')"
HYPERVISOR_HOST="$(onevm show ${VM_NAME} | awk '/^HOST/{ print $3; }')"
SSH="ssh ${HYPERVISOR_HOST}"

# The hard disk image name in CEPH, which should be snapshotted
IMAGE="$(oneimage show ${IMG_NAME} | awk '/SOURCE/{ print $3; }')"

# The hard disk image name in CEPH, which should be snapshotted
PERSISTENT="$(oneimage show ${IMG_NAME} | awk '/^PERSISTENT/{ print $3; }')"
[ "${PERSISTENT}" = "no" ] && exit 3

echo "VM name: ${VM_NAME}"
echo "Hypervisor host for VM: ${HYPERVISOR_HOST}"
echo "Image name: ${VM_NAME}"
echo "Ceph image name: ${IMAGE}"

if [[ ${IMAGE} = */one* ]]; then
	declare -a SNAPSHOTS
	# Read snapshots name to array
	while read ID SNAP SIZE OTHER; do
		[[ ${SNAP} = "NAME" ]] && continue
		SNAPSHOTS+=(${SNAP})
	done < <(${SSH} rbd -n client.oneadmin snap list ${IMAGE})
	# Drop last requested count of snapshots from array - we want to keep them
	for i in $(seq 1 ${NUM}); do
		unset 'SNAPSHOTS[${#SNAPSHOTS[@]}-1]' 2>/dev/null
	done
	# Drop older snapshots
	for SNAP in "${SNAPSHOTS[@]}"; do
		echo "Removing snapshot ${SNAP}..."
		${SSH} rbd -n client.oneadmin snap rm ${IMAGE}@${SNAP}
		echo "- Done removing snapshot ${SNAP}"
	done
fi
