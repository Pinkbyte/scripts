#!/bin/bash
# Check if requested parameters supplied
[ $# -ne 1 ] && exit 1
# Check if script is executed from right username
[ "${USER}" = oneadmin ] || exit 2

# Snapshot will be done only for first attached hard drive
VM_NAME="$1"
IMG_NAME="$(onevm show ${VM_NAME} | awk '/sda/{ print $4; }')"
HYPERVISOR_HOST="$(onevm show ${VM_NAME} | awk '/^HOST/{ print $3; }')"
SSH="ssh ${HYPERVISOR_HOST}"
VIRSH="virsh -c qemu+ssh://${HYPERVISOR_HOST}/system"

# The VM name in libvirt to control
#DEPLOY_ID="$(onevm show ${VM_NAME} | awk '/DEPLOY ID/{ print $4; }')"
#DEPLOY_ID="$(onevm show ${VM_NAME} | awk '/DEPLOY ID/{ print gensub(/'\''/,"","g",$4); }')"
DEPLOY_ID="$(onevm show ${VM_NAME} | awk '/^ID/{ print "one-"$3; exit; }')"

# The hard disk image name in CEPH, which should be snapshotted
IMAGE="$(oneimage show ${IMG_NAME} | awk '/SOURCE/{ print $3; }')"

# The hard disk image name in CEPH, which should be snapshotted
PERSISTENT="$(oneimage show ${IMG_NAME} | awk '/^PERSISTENT/{ print $3; }')"
[ "${PERSISTENT}" = "no" ] && exit 3

echo "VM name: ${VM_NAME}"
echo "Hypervisor host for VM: ${HYPERVISOR_HOST}"
echo "Image name: ${VM_NAME}"
echo "VM deploy id: ${DEPLOY_ID}"
echo "Ceph image name: ${IMAGE}"
echo

if [[ ${IMAGE} = */one* ]]; then
	${VIRSH} qemu-agent-command ${DEPLOY_ID} '{"execute":"guest-fsfreeze-freeze"}'
	if [ $? -eq 0 ]; then
		${VIRSH} qemu-agent-command ${DEPLOY_ID} '{"execute":"guest-fsfreeze-status"}'
		SNAPSHOT_DATE="$(date +'%Y-%m-%d,%H:%M')"
		${SSH} rbd -n client.oneadmin snap create ${IMAGE}@${SNAPSHOT_DATE}
		echo "Snapshot ${IMAGE}@${SNAPSHOT_DATE} is successfully created!"
	fi
	${VIRSH} qemu-agent-command ${DEPLOY_ID} '{"execute":"guest-fsfreeze-thaw"}'
	${VIRSH} qemu-agent-command ${DEPLOY_ID} '{"execute":"guest-fsfreeze-status"}'
fi
