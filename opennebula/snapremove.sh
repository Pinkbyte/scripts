#!/bin/bash
# Check if requested parameters supplied
[ $# -ne 2 ] && exit 1
# Check if script is executed from right username
[ "${USER}" = oneadmin ] || exit 2

# Snapshot will be done only for first attached hard drive
VM_NAME="$1"
SNAPSHOT_NAME="$2"
IMG_NAME="$(onevm show ${VM_NAME} | awk '/sda/{ print $4; }')"

# The hard disk image name in CEPH, where snapshot should be deleted
IMAGE="$(oneimage show ${IMG_NAME} | awk '/SOURCE/{ print $3; }')"
PERSISTENT="$(oneimage show ${IMG_NAME} | awk '/^PERSISTENT/{ print $3; }')"
[ "${PERSISTENT}" = "no" ] && exit 3

echo "VM name: ${VM_NAME}"
echo "Image name: ${VM_NAME}"
echo "Ceph image name: ${IMAGE}"
echo "Snapshot name: ${SNAPSHOT_NAME}"
echo

if [[ ${IMAGE} = */one* ]]; then
	rbd -n client.oneadmin snap remove ${IMAGE}@${SNAPSHOT_NAME}
	if [[ $? -eq 0 ]]; then
		echo "Snapshot ${IMAGE}@${SNAPSHOT_NAME} is successfully deleted!"
	fi
	echo 'Remaining snapshots left: '
	rbd -n client.oneadmin snap list ${IMAGE}
	echo
fi
