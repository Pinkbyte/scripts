#!/bin/bash
# Script for system's suspend when all torrents in Transmission are downloaded
# Copyright: Sergey Popov <admin@pinkbyte.ru>
# Distributed under GNU GPL

if [ -n "`pidof kdesktop`" ]; then
if [ -n "`pidof transmission-daemon`" ]; then
if [ -n "`pidof qtr`" ]; then
	exit 0
else
	export DISPLAY=:0
	CHECK_COMMAND=`transmission-remote --list | cut -c8-11 | grep -v "   " | grep -v 100`
	if [ "$CHECK_COMMAND" = "Done" ]; then
		# Все торренты закачались...
		dbus-launch notify-send "Сообщение от transmission.sh" "Комп уйдет в спящий режим через 3 минуты!"
		sleep 180
		sudo pm-suspend
	fi
fi
fi
fi
